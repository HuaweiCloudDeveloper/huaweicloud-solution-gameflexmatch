module fake-server

go 1.16

require (
	github.com/satori/go.uuid v1.2.0
	go.uber.org/zap v1.21.0
	google.golang.org/grpc v1.46.0
	google.golang.org/protobuf v1.28.0
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
)
