# fake-server

## 启动参数
+ start-port: 开始端口范围；
+ end-port: 结束端口；
+ session-retain-time: 会话保留时间，单位min；
+ process-run-time: 进程运行时间(随机上下50s)，单位min；
+ + max-game-session-count: 进程最大会话数量
+ fake-server: 直接运行二进制文件；
